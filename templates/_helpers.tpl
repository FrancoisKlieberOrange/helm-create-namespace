{{/*
Create image name and tag used by the deployment.
*/}}
{{- define "deleteHook.image" -}}
{{- $fullOverride := .Values.image.fullOverride | default "" -}}
{{- $registry := .Values.global.imageRegistry | default .Values.image.registry -}}
{{- $repository := .Values.image.repository -}}
{{- $separator := ":" -}}
{{- $tag := .Values.image.tag | toString -}}
{{- if $fullOverride }}
    {{- printf "%s" $fullOverride -}}
{{- else if $registry }}
    {{- printf "%s/%s%s%s" $registry $repository $separator $tag -}}
{{- else -}}
    {{- printf "%s%s%s" $repository $separator $tag -}}
{{- end -}}
{{- end -}}
